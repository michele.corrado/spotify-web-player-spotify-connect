//RICERCA ELEMENTI SPOTIFY
const token = "BQBmAd5_bsTr8EH_65qJU-xR-ggR47TtdGBJ4Hggo7i26aGzFsp8Eew8696RvEB5jT5BKZffVv86BWw7xzDGqzqdQaUqbiOUSPeGyQhPoxLBThrpX2hin-KGIrX22AzS4euyOzc9gwoP8ZbN-dBkjOmnDury2GGzbw";

let keywords;
let type;
let limit;
var url;

document.getElementById("search").addEventListener('click', ricerca);

function ricerca() {
    
    keywords = document.getElementById("keywords").value;
    type = document.getElementById("type").value;
    limit = document.getElementById("limit").value;
    url = "https://api.spotify.com/v1/search?q=" + keywords + "&type=" + type + "&limit=" + limit;
    
    fetch(url, {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` }
    })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            console.log(data);

            $("#div1").empty();

            let restituiti;

            if (type == "track") {
                restituiti = data.tracks.items;
                var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-3";
                    var col2 = document.createElement("div");
                    col2.className = "col-3";
                    var col3 = document.createElement("div");
                    col3.className = "col-3";
                    var col4 = document.createElement("div");
                    col4.className = "col-3";

                    var desc1 = document.createElement("p");
                    desc1.style.fontSize = "20px";
                    var desc2 = document.createElement("p");
                    desc2.style.fontSize = "20px";
                    var desc3 = document.createElement("p");
                    desc3.style.fontSize = "20px";
                    

                    desc1.innerText = "Song name";
                    col2.appendChild(desc1);
                    desc2.innerText = "Album";
                    col3.appendChild(desc2);
                    desc3.innerText = "Artist";
                    col4.appendChild(desc3);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);
                    div.appendChild(col4);

                    document.getElementById("div1").appendChild(div);

                restituiti.forEach(element => {
                    
                    var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-3";
                    var col2 = document.createElement("div");
                    col2.className = "col-3";
                    var col3 = document.createElement("div");
                    col3.className = "col-3";
                    var col4 = document.createElement("div");
                    col4.className = "col-3";

                    var img = document.createElement("img");
                    var song = document.createElement("p");
                    var album = document.createElement("p");
                    var artist = document.createElement("p");
                    
                    img.setAttribute("src", element.album.images[0].url);
                    img.style.length = "100px";
                    img.style.height = "100px";
                    col1.appendChild(img);
                    song.innerText = element.name;
                    col2.appendChild(song);
                    album.innerText = element.album.name;
                    col3.appendChild(album);
                    artist.innerText = element.artists[0].name;
                    col4.appendChild(artist);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);
                    div.appendChild(col4);

                    document.getElementById("div1").appendChild(div);

                    console.log(element);
                });
            }

            else if (type == "artist") {
                restituiti = data.artists.items;
                var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-4";
                    var col2 = document.createElement("div");
                    col2.className = "col-4";
                    var col3 = document.createElement("div");
                    col3.className = "col-4";

                    var desc1 = document.createElement("p");
                    desc1.style.fontSize = "20px";
                    var desc2 = document.createElement("p");
                    desc2.style.fontSize = "20px";

                    desc1.innerText = "Artist name";
                    col2.appendChild(desc1);
                    desc2.innerText = "Followers";
                    col3.appendChild(desc2);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);

                    document.getElementById("div1").appendChild(div);

                restituiti.forEach(element => {
                    var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-4";
                    var col2 = document.createElement("div");
                    col2.className = "col-4";
                    var col3 = document.createElement("div");
                    col3.className = "col-4";

                    var img = document.createElement("img");
                    var name = document.createElement("p");
                    var followers = document.createElement("p");

                    img.setAttribute("src", element.images[0].url);
                    img.style.length = "100px";
                    img.style.height = "100px";
                    col1.appendChild(img);
                    name.innerText = element.name;
                    col2.appendChild(name);
                    followers.innerText = element.followers.total;
                    col3.appendChild(followers);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);

                    document.getElementById("div1").appendChild(div);

                    console.log(element);
                });
            }

            else if (type == "album") {
                restituiti = data.albums.items;
                var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-4";
                    var col2 = document.createElement("div");
                    col2.className = "col-4";
                    var col3 = document.createElement("div");
                    col3.className = "col-4";

                    var desc1 = document.createElement("p");
                    desc1.style.fontSize = "20px";
                    var desc2 = document.createElement("p");
                    desc2.style.fontSize = "20px";

                    desc1.innerText = "Album name";
                    col2.appendChild(desc1);
                    desc2.innerText = "Artist";
                    col3.appendChild(desc2);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);

                    document.getElementById("div1").appendChild(div);

                restituiti.forEach(element => {

                    var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-4";
                    var col2 = document.createElement("div");
                    col2.className = "col-4";
                    var col3 = document.createElement("div");
                    col3.className = "col-4";

                    var img = document.createElement("img");
                    var name = document.createElement("p");
                    var followers = document.createElement("p");

                    img.setAttribute("src", element.images[0].url);
                    img.style.length = "100px";
                    img.style.height = "100px";
                    col1.appendChild(img);
                    name.innerText = element.name;
                    col2.appendChild(name);
                    followers.innerText = element.artists[0].name;
                    col3.appendChild(followers);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);

                    document.getElementById("div1").appendChild(div);

                    console.log(element);
                });
            }

            else if (type == "playlist") {
                restituiti = data.playlists.items;

                var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-3";
                    var col2 = document.createElement("div");
                    col2.className = "col-3";
                    var col3 = document.createElement("div");
                    col3.className = "col-3";
                    var col4 = document.createElement("div");
                    col4.className = "col-3";

                    var desc1 = document.createElement("p");
                    desc1.style.fontSize = "20px";
                    var desc2 = document.createElement("p");
                    desc2.style.fontSize = "20px";
                    var desc3 = document.createElement("p");
                    desc3.style.fontSize = "20px";
                    

                    desc1.innerText = "Playlist name";
                    col2.appendChild(desc1);
                    desc2.innerText = "Owner";
                    col3.appendChild(desc2);
                    desc3.innerText = "N° of tracks";
                    col4.appendChild(desc3);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);
                    div.appendChild(col4);

                    document.getElementById("div1").appendChild(div);

                restituiti.forEach(element => {

                    var div = document.createElement("div");
                    div.className = "row";
                    div.style.marginBottom = "10px";

                    var col1 = document.createElement("div");
                    col1.className = "col-3";
                    var col2 = document.createElement("div");
                    col2.className = "col-3";
                    var col3 = document.createElement("div");
                    col3.className = "col-3";
                    var col4 = document.createElement("div");
                    col4.className = "col-3";

                    var img = document.createElement("img");
                    var name = document.createElement("p");
                    var owner = document.createElement("p");
                    var songNumber = document.createElement("p");
                    
                    img.setAttribute("src", element.images[0].url);
                    img.style.length = "100px";
                    img.style.height = "100px";
                    col1.appendChild(img);
                    name.innerText = element.name;
                    col2.appendChild(name);
                    owner.innerText = element.owner.display_name;
                    col3.appendChild(owner);
                    songNumber.innerText = element.tracks.total;
                    col4.appendChild(songNumber);

                    div.appendChild(col1);
                    div.appendChild(col2);
                    div.appendChild(col3);
                    div.appendChild(col4);

                    document.getElementById("div1").appendChild(div);


                    console.log(element);
                });
            }

            else {
                var div = document.createElement("div");
                var error = document.createElement("h1");

                error.innerText = "ERRORE! PARAMETRI MANCANTI O ERRATI";
                error.style.color = "red";
                div.appendChild(error);

                document.getElementById("div1").appendChild(div);
            }
        })
}

//SPOTIFY CONNECT
window.onSpotifyWebPlaybackSDKReady = () => {
    const player = new Spotify.Player({
        name: 'Web Playback SDK Quick Start Player',
        getOAuthToken: cb => { cb(token); }
    });

    //variabili canzone precedente
    let prevArtistName = "";
    let prevSongName = "";
    let prevImgUrl = "";

    //variabili canzone corrente
    let currArtistName = "";
    let currSongName = "";
    let currImgUrl = "";

    //variabili canzone successiva
    let nextArtistName = "";
    let nextSongName = "";
    let nextImgUrl = "";

    // Error handling
    player.addListener('initialization_error', ({ message }) => { console.error(message); });
    player.addListener('authentication_error', ({ message }) => { console.error(message); });
    player.addListener('account_error', ({ message }) => { console.error(message); });
    player.addListener('playback_error', ({ message }) => { console.error(message); });

    // Playback status updates
    player.addListener('player_state_changed', state => {
        console.log(state);

        //assegnazione parametri live canzone precedente
        prevArtistName = state.track_window.previous_tracks[0].artists[0].name;
        prevSongName = state.track_window.previous_tracks[0].name;
        prevImgUrl = state.track_window.previous_tracks[0].album.images[0].url;

        document.getElementById("prevSongName").innerHTML = prevSongName;
        document.getElementById("prevImage").src = prevImgUrl;
        document.getElementById("prevArtistName").innerHTML = prevArtistName;

        //assegnazione parametri live canzone corrente
        currArtistName = state.track_window.current_track.artists[0].name;
        currSongName = state.track_window.current_track.name;
        currImgUrl = state.track_window.current_track.album.images[0].url;

        document.getElementById("currSongName").innerHTML = currSongName;
        document.getElementById("currImage").src = currImgUrl;
        document.getElementById("currArtistName").innerHTML = currArtistName;

        //assegnazione parametri live canzone successiva
        nextArtistName = state.track_window.next_tracks[0].artists[0].name;
        nextSongName = state.track_window.next_tracks[0].name;
        nextImgUrl = state.track_window.next_tracks[0].album.images[0].url;

        document.getElementById("nextSongName").innerHTML = nextSongName;
        document.getElementById("nextImage").src = nextImgUrl;
        document.getElementById("nextArtistName").innerHTML = nextArtistName;
    });

    // Ready
    player.addListener('ready', ({ device_id }) => {
        console.log('Ready with Device ID', device_id);
    });

    // Not Ready
    player.addListener('not_ready', ({ device_id }) => {
        console.log('Device ID has gone offline', device_id);
    });

    // Connect to the player!
    player.connect();

};